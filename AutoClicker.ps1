﻿<# Simple Windows PowerShell auto clicker

     Copyright (C) 2023 StarkNet
     Licensed under GPLv3 or any later version
     Refer to the LICENCE file included

   Thanks for the question on Stack Overflow!
     <https://stackoverflow.com/questions/38225874/mouse-click-automation-with-powershell-or-other-non-external-software>
 #>

# Get script name
$ScriptName = $MyInvocation.MyCommand.Name
$ScriptName = [System.IO.Path]::GetFileNameWithoutExtension($ScriptName)

# Importing a couple of system assemblies
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

# Creation of array by calling in 'user32.dll' and the mouse clicking event
$signature=@'
[DllImport("User32.dll",CharSet=CharSet.Auto,CallingConvention=CallingConvention.StdCall)]
public static extern void mouse_event(long dwFlags, long dx, long dy, long cButtons, long dwExtraInfo);
'@

# Function to get the state of the keyboard keys
Add-Type -TypeDefinition @"
    using System;
    using System.Runtime.InteropServices;

    public class Keyboard {
        [DllImport("User32.dll")]
        public static extern short GetAsyncKeyState(int vKey);
    }
"@

# Add a type of the above called mouse event array list
$SendMouseClick = Add-Type -MemberDefinition $signature -Name "Win32MouseEventNew" -Namespace Win32Functions -PassThru

# Initialization
$time = 100    # default time for click interval
$flag = $false # passage indicator for click activation

# Argument check
if ($args.Count -ge 1) {
    $value  = $args[0]
    $result = $false

    # Exception handling
    if ([int]::TryParse($value, [ref]$result) -and $result -gt 0) {
        $time = $value # valid argument
    } else {
        Write-Host "/!\ Incorrect, default value`n" -ForegroundColor Red
    }
}

# Presentation message
$message = @"
$ScriptName  Copyright (C) 2023  StarkNet
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions.

> F1 to enable auto clicker
> F2 to enable auto clicker with pressed key
> Esc to stop

> You can add your custom time interval by specifying an argument in milliseconds.
  (ex : .\$ScriptName.ps1 42)

Current click interval is $time milliseconds
"@

Write-Host -Object $message

# Click loop
while ($true) {
    Start-Sleep -Milliseconds $time # click interval

    # Cursor position
    $x = [System.Windows.Forms.Cursor]::Position.X
    $y = [System.Windows.Forms.Cursor]::Position.Y

    [System.Windows.Forms.Cursor]::Position = New-Object System.Drawing.Point($x, $y)

    # Get keys state to autoclicker control
    $keyStateF1  = [Keyboard]::GetAsyncKeyState(0x70)
    $keyStateF2  = [Keyboard]::GetAsyncKeyState(0x71)
    $keyStateEsc = [Keyboard]::GetAsyncKeyState(0x1B)

    if ($keyStateF1 -eq -32767 -and $flag -eq $false) {
        $flag = $true
    }

    # Single press of the escape key
    elseif ($keyStateEsc -eq -32767) {
        $flag = $false # disable clicks for F1 key
        Start-Sleep -Seconds 1
    }

    # Hold F2 or just press F1
    elseif ($keyStateF2 -eq -32767 -or $flag -eq $true) {
        $SendMouseClick::mouse_event(0x00000002, 0, 0, 0, 0); # left click down
        $SendMouseClick::mouse_event(0x00000004, 0, 0, 0, 0); # left click up
    }
}
