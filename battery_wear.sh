#!/bin/bash
# Simple Bash script to get battery wear

# Copyright (C) 2023 StarkNet
# Licensed under GPLv3 or any later version
# Refer to the LICENCE file included

# Number of batteries
num_batteries=$(ls /sys/class/power_supply/ | grep '^BAT' | wc -l)

# No battery detected
if [[ "$num_batteries" -eq 0 ]]; then
    echo "No battery detected"
    exit 1
else
    # For each battery
    for i in $(seq 0 $((num_batteries - 1))); do
	battery_name="BAT${i}"

	# Existence of energy or charge file
	if [ -f "/sys/class/power_supply/$battery_name/energy_full" ]; then
	    full=$(cat "/sys/class/power_supply/$battery_name/energy_full")
	    full_design=$(cat "/sys/class/power_supply/$battery_name/energy_full_design")
	else
	    full=$(cat "/sys/class/power_supply/$battery_name/charge_full")
	    full_design=$(cat "/sys/class/power_supply/$battery_name/charge_full_design")
	fi

	# Battery wear rate calculation in percentage and display
	wear_level=$((100 - $full * 100 / $full_design))
        echo "Battery $i: Wear $wear_level%"
    done
fi
