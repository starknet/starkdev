# StarkDev

# Introduction

Free software has emerged as a transformative movement in the world
of technology. In an era where proprietary software dominates the
market, free software offers a different approach. Rooted in the
philosophy of user freedom, it grants individuals the liberty to
use, study, modify, and distribute the software according to their
needs. This ethos of openness and collaboration has given rise to
a vibrant ecosystem of projects, spanning from operating systems to
applications. By embracing the principles of transparency, inclusivity,
and user empowerment, free software has not only democratized access to
technology but has also nurtured innovation and fostered communities.

Join the movement and become part of a global community
dedicated to advancing technology for the benefit of all!

This project is under the
  [GNU General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0.html).
Please read this license agreement carefully.

# Softwares

## [AutoClicker.ps1](https://gitlab.com/starknet/starkdev/-/blob/main/AutoClicker.ps1)

Simple Windows PowerShell auto clicker. To use it, it's better to
run it in a PowerShell console to add the interval argument between
each click.

```powershell
.\AutoClicker.ps1 42
```

For example, I specified an interval of 42 milliseconds between each
click. If there is no argument or is invalid, it will default to
**100 milliseconds**.

Finally, how to use the script:
  * `F1` to enable auto clicker
  * `F2` to enable auto clicker with pressed key
  * `Esc` to stop

## [battery_wear.sh](https://gitlab.com/starknet/starkdev/-/blob/main/battery_wear.sh)

Simple Bash script to get battery wear. This script works on all
recent GNU/Linux systems using the **kernel power class**.
To use it, simply run the script.
